from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('register/', views.registerUser, name='register'),
    path('account/', views.getUserAccount, name = 'account'),
    path('account/update/', views.updateUserAccount, name = 'account-update'),
    path('', views.getUsers, name='users'),
]
